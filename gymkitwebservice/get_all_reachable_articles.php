<?php

    $response = array();

    if (isset($_POST['gymid']))
    {
        $gymid = $_POST['gymid'];

        require_once('db_config.php');

        $result = $db->query("SELECT * FROM article WHERE gymid = $gymid OR gymid = 0 ORDER BY id DESC")->fetchAll(PDO::FETCH_ASSOC);

        if(isset($result[0]['id']))
        {
            $response['article'] = array();

            foreach ($result as $row)
            {
                $article = array();

                $article['id'] = $row['id'];
                $article['title'] = $row['title'];
                $article['photo'] = $row['photo'];
                $article['content'] = $row['content'];
                $article['date'] = $row['date'];

                array_push($response['article'], $article);
            }

            $response['success'] = 1;

            echo json_encode($response);
        }

        else
        {
            $response['success'] = 0;
            $response['message'] = "No data found!";
            echo json_encode($response);
        }

        $db = null;

    }

    else
    {
        $response['success'] = 0;
        $response['message'] = "Posted variables are wrong!";
        echo json_encode($response);
    }

?>