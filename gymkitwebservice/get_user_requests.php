<?php

    $response = array();
    
    if (isset($_POST['gymid']))
    {
        $gymid = $_POST['gymid'];
    
        require_once('db_config.php');
    
        $result = $db->query("SELECT * FROM user WHERE gymid = $gymid AND activation = 0")->fetchAll(PDO::FETCH_ASSOC);
    
        if(isset($result[0]['id']))
        {
            $response['user'] = array();
    
            foreach ($result as $row)
            {
                $user = array();
    
                $user['id'] = $row['id'];
                $user['fname'] = $row['fname'];
                $user['lname'] = $row['lname'];
                $user['email'] = $row['email'];
                $user['password'] = $row['password'];
                $user['phone'] = $row['phone'];
                $user['gender'] = $row['gender'];
                $user['height'] = $row['height'];
                $user['bday'] = $row['bday'];
                $user['gymid'] = $row['gymid'];
                $user['usertype'] = $row['usertype'];
                $user['activation'] = $row['activation'];
    
                array_push($response['user'], $user);
            }
    
            $response['success'] = 1;
    
            echo json_encode($response);
        }
    
        else
        {
            $response['success'] = 0;
            $response['message'] = "No data found!";
            echo json_encode($response);
        }
    
        $db = null;
    
    }
    
    else
    {
        $response['success'] = 0;
        $response['message'] = "Posted variables are wrong!";
        echo json_encode($response);
    }

?>