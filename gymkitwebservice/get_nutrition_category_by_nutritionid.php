<?php

    $response = array();

    if (isset($_POST['nutritionid']))
    {
        $nutritionid = $_POST['nutritionid'];

        require_once('db_config.php');

        $result = $db->query("SELECT * FROM nutrition_category WHERE nutritionid = $nutritionid")->fetchAll(PDO::FETCH_ASSOC);

        $response['category'] = array();

        $category = array();

        $category['id'] = 0;
        $category['nutritionid'] = $nutritionid;
        $category['name'] = "Continue Without Choosing Category";

        array_push($response['category'], $category);

        if(isset($result[0]['id']))
        {

            foreach ($result as $row)
            {
                $category = array();

                $category['id'] = $row['id'];
                $category['nutritionid'] = $row['nutritionid'];
                $category['name'] = $row['name'];

                array_push($response['category'], $category);
            }

            $response['success'] = 1;

            echo json_encode($response);
        }

        else
        {
            $response['success'] = 0;
            $response['message'] = "No data found!";
            echo json_encode($response);
        }

        $db = null;

    }

    else
    {
        $response['success'] = 0;
        $response['message'] = "Posted variables are wrong!";
        echo json_encode($response);
    }

?>