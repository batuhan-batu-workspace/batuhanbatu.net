<?php

    $response = array();

    if (isset($_POST['id']))
    {
        $id = $_POST['id'];

        require_once('db_config.php');

        $result = $db->query("SELECT * FROM article WHERE id = $id")->fetchAll(PDO::FETCH_ASSOC);

        if(isset($result[0]['id']))
        {
            $response['article'] = array();

            foreach ($result as $row)
            {
                $article = array();
                
                $article['id'] = $row['id'];
                $article['title'] = $row['title'];
                $article['photo'] = $row['photo'];
                $article['content'] = $row['content'];
                $article['date'] = $row['date'];

                $authorid = $row['authorid'];
                $result2 = $db->query("SELECT * FROM user WHERE id = $authorid")->fetchAll(PDO::FETCH_ASSOC);
                foreach ($result2 as $row2)
                {
                    $article['authorname'] = $row2['fname'].' '.$row2['lname'];
                }

                array_push($response['article'], $article);
            }

            $response['success'] = 1;

            echo json_encode($response);
        }

        else
        {
            $response['success'] = 0;
            $response['message'] = "No data found!";
            echo json_encode($response);
        }

        $db = null;

    }

    else
    {
        $response['success'] = 0;
        $response['message'] = "Posted variables are wrong!";
        echo json_encode($response);
    }

?>