<?php

    $response = array();

    if (isset($_POST['articleid']) and isset($_POST['userid']))
    {
        $articleid = $_POST['articleid'];
        $userid = $_POST['userid'];

        require_once('db_config.php');

        $result = $db->query("SELECT * FROM article WHERE id = $articleid")->fetchAll(PDO::FETCH_ASSOC);

        $response['connection'] = array();

        foreach ($result as $row)
        {
            $connection = array();

            if ($row['authorid'] == $userid)
                $connection['owner'] = 1;
            else
                $connection['owner'] = 0;

            $result2 = $db->query("SELECT * FROM saved_article WHERE articleid = $articleid AND userid = $userid")->fetchAll(PDO::FETCH_ASSOC);

            if (isset($result2[0]['userid']))
            {
                foreach ($result2 as $row2)
                    $connection['favorite'] = 1;
            }
            else
                $connection['favorite'] = 0;

            array_push($response['connection'], $connection);
        }

        $response['success'] = 1;

        echo json_encode($response);

        $db = null;

    }

    else
    {
        $response['success'] = 0;
        $response['message'] = "Posted variables are wrong!";
        echo json_encode($response);
    }

?>