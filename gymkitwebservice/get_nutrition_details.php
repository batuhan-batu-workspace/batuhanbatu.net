<?php

    $response = array();

    if (isset($_POST['nutritionid']))
    {
        $nutritionid = $_POST['nutritionid'];
        $creatorid = 0;

        $response['detail'] = array();

        require_once('db_config.php');

        $result = $db->query("SELECT * FROM nutrition WHERE id = $nutritionid")->fetchAll(PDO::FETCH_ASSOC);

        if(isset($result[0]['id']))
        {
            foreach ($result as $row)
            {
                $creatorid = $row['creatorid'];
            }
        }



        //If there exists meals without having a category

        $result = $db->query("SELECT * FROM nutrition_meal WHERE nutritionid = $nutritionid AND categoryid = 0 ORDER BY id ASC")->fetchAll(PDO::FETCH_ASSOC);

        if(isset($result[0]['id']))
        {
            foreach ($result as $row)
            {
                $meal = array();
                
                $meal['type'] = 1;
                $meal['id'] = $row['id'];
                $meal['name'] = $row['name'];
                $meal['content'] = $row['content'];
                $meal['creatorid'] = $creatorid;

                array_push($response['detail'], $meal);

            }
        }

        //Other exercises having category
        $result = $db->query("SELECT * FROM nutrition_category WHERE nutritionid = $nutritionid ORDER BY id ASC")->fetchAll(PDO::FETCH_ASSOC);

        if(isset($result[0]['id']))
        {

            foreach ($result as $row)
            {
                $category = array();

                $categoryid = $row['id'];

                $category['type'] = 0;
                $category['id'] = $row['id'];
                $category['name'] = $row['name'];
                $category['creatorid'] = $creatorid;

                array_push($response['detail'], $category);


                $result2 = $db->query("SELECT * FROM nutrition_meal WHERE nutritionid = $nutritionid AND categoryid = $categoryid ORDER BY id ASC")->fetchAll(PDO::FETCH_ASSOC);

                if(isset($result2[0]['id']))
                {
                    foreach ($result2 as $row2)
                    {
                        $meal = array();

                        $meal['type'] = 1;
                        $meal['id'] = $row2['id'];
                        $meal['name'] = $row2['name'];
                        $meal['content'] = $row2['content'];
                        $meal['creatorid'] = $creatorid;

                        array_push($response['detail'], $meal);

                    }
                }
            }

            $response['success'] = 1;

            echo json_encode($response);

        }

        else
        {
            $response['success'] = 0;
            $response['message'] = "No data found!";
            echo json_encode($response);
        }

        $db = null;

    }

    else
    {
        $response['success'] = 0;
        $response['message'] = "Posted variables are wrong!";
        echo json_encode($response);
    }

?>