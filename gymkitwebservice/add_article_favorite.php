<?php

    $response = array();

    if (isset($_POST['articleid']) and isset($_POST['userid']))
    {
        $articleid = $_POST['articleid'];
        $userid = $_POST['userid'];

        require_once('db_config.php');


        if ($db->query("INSERT INTO saved_article (articleid, userid) VALUES ($articleid, $userid)"))
        {
            $response['success'] = 1;
            $response['message'] = "Successfully added!";

            echo json_encode($response);
        }

        else
        {
            $response['success'] = 0;
            $response['message'] = "Couldn't add!";
            echo json_encode($response);
        }

        $db = null;
    }

    else
    {
        $response['success'] = 0;
        $response['message'] = "Posted variables are wrong!";
        echo json_encode($response);
    }


?>