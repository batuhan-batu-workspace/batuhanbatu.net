<?php

    $response = array();

    if (isset($_POST['id']))
    {
        $id = $_POST['id'];

        require_once('db_config.php');


        if ($db->query("DELETE FROM workout_category WHERE id = $id") and $db->query("DELETE FROM workout_exercise WHERE categoryid = $id"))
        {
            $response['success'] = 1;
            $response['message'] = "Successfully deleted!";

            echo json_encode($response);
        }

        else
        {
            $response['success'] = 0;
            $response['message'] = "Couldn't delete!";
            echo json_encode($response);
        }

        $db = null;
    }

    else
    {
        $response['success'] = 0;
        $response['message'] = "Posted variables are wrong!";
        echo json_encode($response);
    }


?>