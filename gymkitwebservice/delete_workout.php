<?php

    $response = array();

    if (isset($_POST['workoutid']))
    {
        $workoutid = $_POST['workoutid'];

        require_once('db_config.php');


        if ($db->query("DELETE FROM workout WHERE id = $workoutid") and $db->query("DELETE FROM workout_category WHERE workoutid = $workoutid") and $db->query("DELETE FROM workout_exercise WHERE workoutid = $workoutid"))
        {
            $response['success'] = 1;
            $response['message'] = "Successfully deleted!";

            echo json_encode($response);
        }

        else
        {
            $response['success'] = 0;
            $response['message'] = "Couldn't delete!";
            echo json_encode($response);
        }

        $db = null;
    }

    else
    {
        $response['success'] = 0;
        $response['message'] = "Posted variables are wrong!";
        echo json_encode($response);
    }


?>