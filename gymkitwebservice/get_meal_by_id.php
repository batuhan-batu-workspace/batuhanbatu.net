<?php

    $response = array();

    if (isset($_POST['id']))
    {
        $id = $_POST['id'];

        require_once('db_config.php');

        $result = $db->query("SELECT * FROM nutrition_meal WHERE id = $id")->fetchAll(PDO::FETCH_ASSOC);

        if(isset($result[0]['id']))
        {
            $response['meal'] = array();

            foreach ($result as $row)
            {
                $meal = array();

                $meal['id'] = $row['id'];
                $meal['nutritionid'] = $row['nutritionid'];
                $meal['name'] = $row['name'];
                $meal['content'] = $row['content'];
                $meal['categoryid'] = $row['categoryid'];

                array_push($response['meal'], $meal);
            }

            $response['success'] = 1;

            echo json_encode($response);
        }

        else
        {
            $response['success'] = 0;
            $response['message'] = "No data found!";
            echo json_encode($response);
        }

        $db = null;

    }

    else
    {
        $response['success'] = 0;
        $response['message'] = "Posted variables are wrong!";
        echo json_encode($response);
    }

?>