<?php

    $response = array();

    if (isset($_POST['id']))
    {
        $id = $_POST['id'];
        $userid = 0;
        $usergender = 0;
        $userheight = 0;

        require_once('db_config.php');

        $result = $db->query("SELECT * FROM body_size WHERE id = $id")->fetchAll(PDO::FETCH_ASSOC);

        if(isset($result[0]['id']))
        {
            $response['body_size'] = array();

            foreach ($result as $row)
            {
                $userid = $row['userid'];
                $userweight = $row['weight'];
                $result2 = $db->query("SELECT * FROM user WHERE id = $userid")->fetchAll(PDO::FETCH_ASSOC);
                if(isset($result2[0]['id']))
                {
                    foreach ($result2 as $row2)
                    {
                        $usergender = $row2['gender'];
                        $userheight = $row2['height'];
                    }
                }

                $bodysize = array();

                $bodysize['id'] = $row['id'];
                $bodysize['name'] = $row['name'];
                $bodysize['weight'] = $row['weight'];
                $bodysize['fluid'] = $row['fluid'];
                $bodysize['oil'] = $row['oil'];
                $bodysize['muscle'] = $row['muscle'];
                $bodysize['userid'] = $row['userid'];
                $bodysize['creatorid'] = $row['creatorid'];

                if ($usergender == 0)
                {
                    $userheight /= 100;
                    $userheight = ($userweight / ($userheight * $userheight));
                    if ($userheight < 18.5)
                        $bodysize['weightt'] = -1;
                    else if ($userheight < 25)
                        $bodysize['weightt'] = 0;
                    else
                        $bodysize['weightt'] = 1;

                    if ($row['fluid'] < 50)
                        $bodysize['fluidd'] = -1;
                    else if ($row['fluid'] < 60)
                        $bodysize['fluidd'] = 0;
                    else
                        $bodysize['fluidd'] = 1;

                    if ($row['oil'] < 14)
                        $bodysize['oill'] = -1;
                    else if ($row['oil'] < 24)
                        $bodysize['oill'] = 0;
                    else
                        $bodysize['oill'] = 1;

                    if ($row['muscle'] < 40)
                        $bodysize['musclee'] = -1;
                    else if ($row['muscle'] < 65)
                        $bodysize['musclee'] = 0;
                    else
                        $bodysize['musclee'] = 1;
                }

                else if ($usergender == 1)
                {
                    $userheight /= 100;
                    $userheight = ($userweight / ($userheight * $userheight));
                    if ($userheight < 18.5)
                        $bodysize['weightt'] = -1;
                    else if ($userheight < 25)
                        $bodysize['weightt'] = 0;
                    else
                        $bodysize['weightt'] = 1;

                    if ($row['fluid'] < 55)
                        $bodysize['fluidd'] = -1;
                    else if ($row['fluid'] < 65)
                        $bodysize['fluidd'] = 0;
                    else
                        $bodysize['fluidd'] = 1;

                    if ($row['oil'] < 6)
                        $bodysize['oill'] = -1;
                    else if ($row['oil'] < 17)
                        $bodysize['oill'] = 0;
                    else
                        $bodysize['oill'] = 1;

                    if ($row['muscle'] < 50)
                        $bodysize['musclee'] = -1;
                    else if ($row['muscle'] < 75)
                        $bodysize['musclee'] = 0;
                    else
                        $bodysize['musclee'] = 1;
                }

                array_push($response['body_size'], $bodysize);
            }

            $response['success'] = 1;

            echo json_encode($response);
        }

        else
        {
            $response['success'] = 0;
            $response['message'] = "No data found!";
            echo json_encode($response);
        }

        $db = null;

    }

    else
    {
        $response['success'] = 0;
        $response['message'] = "Posted variables are wrong!";
        echo json_encode($response);
    }

?>