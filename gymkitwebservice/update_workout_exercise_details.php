<?php

    $response = array();

    if (isset($_POST['id']) and isset($_POST['setnum']) and isset($_POST['repetition']))
    {
        $id = $_POST['id'];
        $setnum = $_POST['setnum'];
        $repetition = $_POST['repetition'];

        require_once('db_config.php');


        if($db->query("UPDATE workout_exercise SET setnum = $setnum, repetition = $repetition WHERE id = $id"))
        {
            $response['success'] = 1;
            $response['message'] = "Successfully updated!";

            echo json_encode($response);
        }

        else
        {
            $response['success'] = 0;
            $response['message'] = "Error!";
        }



        $db = null;

    }

    else
    {
        $response['success'] = 0;
        $response['message'] = "Posted variables are wrong!";
        echo json_encode($response);
    }

?>