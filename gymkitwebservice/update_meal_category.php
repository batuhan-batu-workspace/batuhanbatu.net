<?php

    $response = array();

    if (isset($_POST['mealid']) and isset($_POST['categoryid']))
    {
        $mealid = $_POST['mealid'];
        $categoryid = $_POST['categoryid'];

        require_once('db_config.php');


        if($db->query("UPDATE nutrition_meal SET categoryid = $categoryid WHERE id = $mealid"))
        {
            $response['success'] = 1;
            $response['message'] = "Successfully updated!";

            echo json_encode($response);
        }

        else
        {
            $response['success'] = 0;
            $response['message'] = "Error!";
        }

        $db = null;

    }

    else
    {
        $response['success'] = 0;
        $response['message'] = "Posted variables are wrong!";
        echo json_encode($response);
    }

?>