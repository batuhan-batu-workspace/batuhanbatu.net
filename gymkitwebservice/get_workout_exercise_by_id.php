<?php

    $response = array();

    if (isset($_POST['id']))
    {
        $id = $_POST['id'];

        require_once('db_config.php');

        $result = $db->query("SELECT * FROM workout_exercise WHERE id = $id")->fetchAll(PDO::FETCH_ASSOC);

        if(isset($result[0]['id']))
        {
            $response['exercise'] = array();

            foreach ($result as $row)
            {
                $exercise = array();

                $exercise['id'] = $row['id'];
                $exercise['workoutid'] = $row['workoutid'];
                $exercise['exerciseid'] = $row['exerciseid'];
                $exercise['setnum'] = $row['setnum'];
                $exercise['repetition'] = $row['repetition'];
                $exercise['categoryid'] = $row['categoryid'];

                array_push($response['exercise'], $exercise);
            }

            $response['success'] = 1;

            echo json_encode($response);
        }

        else
        {
            $response['success'] = 0;
            $response['message'] = "No data found!";
            echo json_encode($response);
        }

        $db = null;

    }

    else
    {
        $response['success'] = 0;
        $response['message'] = "Posted variables are wrong!";
        echo json_encode($response);
    }

?>