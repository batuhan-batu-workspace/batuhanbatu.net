<?php

    $response = array();

    if (isset($_POST['userid']) and isset($_POST['nutritionid']) and isset($_POST['assignerid']))
    {
        $userid = $_POST['userid'];
        $nutritionid = $_POST['nutritionid'];
        $assignerid = $_POST['assignerid'];

        $gymid = 0;
        $nutritionname = "";
        $newnutritionid = 0;

        $oldcategorylist = array();
        $newcategorylist = array();

        require_once('db_config.php');

        $result = $db->query("SELECT * FROM user WHERE id = $assignerid")->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row)
        {
            $gymid = $row['gymid'];
        }

        $result = $db->query("SELECT * FROM nutrition WHERE id = $nutritionid")->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row)
        {
            $nutritionname = $row['name'];
        }

        $db->query("INSERT INTO nutrition (name, userid, creatorid, gymid) VALUES ('$nutritionname', $userid, $assignerid, $gymid)");

        $result = $db->query("SELECT * FROM nutrition WHERE gymid = $gymid AND creatorid = $assignerid AND userid = $userid ORDER BY id ASC")->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row)
        {
            $newnutritionid = $row['id'];
        }


        $result = $db->query("SELECT * FROM nutrition_category WHERE nutritionid = $nutritionid ORDER BY id ASC")->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row)
        {
            array_push($oldcategorylist, $row['id']);
            $categoryname = $row['name'];
            $db->query("INSERT INTO nutrition_category (nutritionid, name) VALUES ($newnutritionid, '$categoryname')");

            $result2 = $db->query("SELECT * FROM nutrition_category WHERE nutritionid = $newnutritionid ORDER BY id ASC")->fetchAll(PDO::FETCH_ASSOC);

            $newaddedcategoryid = 0;

            foreach ($result2 as $row2)
            {
                $newaddedcategoryid = $row2['id'];
            }

            array_push($newcategorylist, $newaddedcategoryid);

        }


        $result = $db->query("SELECT * FROM nutrition_meal WHERE nutritionid = $nutritionid ORDER BY id ASC")->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row)
        {
            $nutritionid = $row['nutritionid'];
            $name = $row['name'];
            $content = $row['content'];

            $oldcategoryid = $row['categoryid'];
            $newcategoryid = 0;

            if ($oldcategoryid != 0)
            {

                for ($i = 0; $i < sizeof($oldcategorylist); $i++)
                {
                    if ($oldcategorylist[$i] == $oldcategoryid)
                        $newcategoryid = $newcategorylist[$i];
                }

            }

            $db->query("INSERT INTO nutrition_meal (nutritionid, name, content, categoryid) VALUES ($newnutritionid, '$name', '$content', $newcategoryid)");

        }


        $response['success'] = 1;
        $response['message'] = "Successfully added!";

        echo json_encode($response);

        $db = null;
    }

    else
    {
        $response['success'] = 0;
        $response['message'] = "Posted variables are wrong!";
        echo json_encode($response);
    }

?>