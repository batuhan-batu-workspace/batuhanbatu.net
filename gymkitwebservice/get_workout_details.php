<?php

    $response = array();

    if (isset($_POST['workoutid']))
    {
        $workoutid = $_POST['workoutid'];
        $creatorid = 0;
        
        $response['detail'] = array();
        
        require_once('db_config.php');

        $result = $db->query("SELECT * FROM workout WHERE id = $workoutid")->fetchAll(PDO::FETCH_ASSOC);

        if(isset($result[0]['id']))
        {
            foreach ($result as $row)
            {
                $creatorid = $row['creatorid'];
            }
        }
        
        
        
        //If there exists exercises without having a category

        $result = $db->query("SELECT * FROM workout_exercise WHERE workoutid = $workoutid AND categoryid = 0 ORDER BY id ASC")->fetchAll(PDO::FETCH_ASSOC);

        if(isset($result[0]['id']))
        {
            foreach ($result as $row)
            {
                $exercise = array();

                $exerciseid = $row['exerciseid'];

                $exercise['type'] = 1;
                $exercise['weid'] = $row['id'];
                $exercise['id'] = $row['exerciseid'];
                $exercise['setnum'] = $row['setnum'];
                $exercise['repetition'] = $row['repetition'];
                $exercise['creatorid'] = $creatorid;

                $result2 = $db->query("SELECT * FROM exercise WHERE id = $exerciseid")->fetchAll(PDO::FETCH_ASSOC);
                if(isset($result2[0]['id']))
                {
                    foreach ($result2 as $row2)
                    {
                        $exercise['name'] = $row2['name'];
                        $exercise['gif'] = $row2['gif'];
                    }
                }

                array_push($response['detail'], $exercise);

            }
        }

        //Other exercises having category
        $result = $db->query("SELECT * FROM workout_category WHERE workoutid = $workoutid ORDER BY id ASC")->fetchAll(PDO::FETCH_ASSOC);

        if(isset($result[0]['id']))
        {

            foreach ($result as $row)
            {
                $category = array();

                $categoryid = $row['id'];

                $category['type'] = 0;
                $category['id'] = $row['id'];
                $category['name'] = $row['name'];
                $category['creatorid'] = $creatorid;

                array_push($response['detail'], $category);


                $result2 = $db->query("SELECT * FROM workout_exercise WHERE workoutid = $workoutid AND categoryid = $categoryid ORDER BY id ASC")->fetchAll(PDO::FETCH_ASSOC);

                if(isset($result2[0]['id']))
                {
                    foreach ($result2 as $row2)
                    {
                        $exercise = array();

                        $exerciseid = $row2['exerciseid'];

                        $exercise['type'] = 1;
                        $exercise['weid'] = $row2['id'];
                        $exercise['id'] = $row2['exerciseid'];
                        $exercise['setnum'] = $row2['setnum'];
                        $exercise['repetition'] = $row2['repetition'];
                        $exercise['creatorid'] = $creatorid;
                        
                        $result3 = $db->query("SELECT * FROM exercise WHERE id = $exerciseid")->fetchAll(PDO::FETCH_ASSOC);
                        if(isset($result3[0]['id']))
                        {
                            foreach ($result3 as $row3)
                            {
                                $exercise['name'] = $row3['name'];
                                $exercise['gif'] = $row3['gif'];
                            }
                        }

                        array_push($response['detail'], $exercise);

                    }
                }
            }

            $response['success'] = 1;

            echo json_encode($response);
            
        }

        else
        {
            $response['success'] = 0;
            $response['message'] = "No data found!";
            echo json_encode($response);
        }

        $db = null;

    }

    else
    {
        $response['success'] = 0;
        $response['message'] = "Posted variables are wrong!";
        echo json_encode($response);
    }

?>