<?php

    $response = array();

    if (isset($_POST['id']))
    {
        $id = $_POST['id'];
    
        require_once('db_config.php');

        $result = $db->query("SELECT * FROM nutrition WHERE id = $id")->fetchAll(PDO::FETCH_ASSOC);

        if(isset($result[0]['id']))
        {
            $response['nutrition'] = array();

            foreach ($result as $row)
            {
                $workout = array();

                $nutrition['id'] = $row['id'];
                $nutrition['name'] = $row['name'];
                $nutrition['userid'] = $row['userid'];
                $nutrition['creatorid'] = $row['creatorid'];
                $nutrition['gymid'] = $row['gymid'];

                array_push($response['nutrition'], $nutrition);
            }

            $response['success'] = 1;

            echo json_encode($response);
        }

        else
        {
            $response['success'] = 0;
            $response['message'] = "No data found!";
            echo json_encode($response);
        }

        $db = null;

    }

    else
    {
        $response['success'] = 0;
        $response['message'] = "Posted variables are wrong!";
        echo json_encode($response);
    }

?>