<?php

    $response = array();

    if (isset($_POST['nutritionid']))
    {
        $nutritionid = $_POST['nutritionid'];

        require_once('db_config.php');


        if ($db->query("DELETE FROM nutrition WHERE id = $nutritionid") and $db->query("DELETE FROM nutrition_category WHERE nutritionid = $nutritionid") and $db->query("DELETE FROM nutrition_meal WHERE nutritionid = $nutritionid"))
        {
            $response['success'] = 1;
            $response['message'] = "Successfully deleted!";

            echo json_encode($response);
        }

        else
        {
            $response['success'] = 0;
            $response['message'] = "Couldn't delete!";
            echo json_encode($response);
        }

        $db = null;
    }

    else
    {
        $response['success'] = 0;
        $response['message'] = "Posted variables are wrong!";
        echo json_encode($response);
    }


?>