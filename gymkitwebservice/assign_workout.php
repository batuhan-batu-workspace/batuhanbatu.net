<?php

    $response = array();

    if (isset($_POST['userid']) and isset($_POST['workoutid']) and isset($_POST['assignerid']))
    {
        $userid = $_POST['userid'];
        $workoutid = $_POST['workoutid'];
        $assignerid = $_POST['assignerid'];

        $gymid = 0;
        $workoutname = "";
        $newworkoutid = 0;

        $oldcategorylist = array();
        $newcategorylist = array();

        require_once('db_config.php');

        $result = $db->query("SELECT * FROM user WHERE id = $assignerid")->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row)
        {
            $gymid = $row['gymid'];
        }

        $result = $db->query("SELECT * FROM workout WHERE id = $workoutid")->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row)
        {
            $workoutname = $row['name'];
        }

        $db->query("INSERT INTO workout (name, userid, creatorid, gymid) VALUES ('$workoutname', $userid, $assignerid, $gymid)");

        $result = $db->query("SELECT * FROM workout WHERE gymid = $gymid AND creatorid = $assignerid AND userid = $userid ORDER BY id ASC")->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row)
        {
            $newworkoutid = $row['id'];
        }
        
        

        $result = $db->query("SELECT * FROM workout_category WHERE workoutid = $workoutid ORDER BY id ASC")->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row)
        {
            array_push($oldcategorylist, $row['id']);
            $categoryname = $row['name'];
            $db->query("INSERT INTO workout_category (workoutid, name) VALUES ($newworkoutid, '$categoryname')");

            $result2 = $db->query("SELECT * FROM workout_category WHERE workoutid = $newworkoutid ORDER BY id ASC")->fetchAll(PDO::FETCH_ASSOC);

            $newaddedcategoryid = 0;

            foreach ($result2 as $row2)
            {
                $newaddedcategoryid = $row2['id'];
            }

            array_push($newcategorylist, $newaddedcategoryid);

        }

        
        $result = $db->query("SELECT * FROM workout_exercise WHERE workoutid = $workoutid ORDER BY id ASC")->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row)
        {
            $exerciseid = $row['exerciseid'];
            $setnum = $row['setnum'];
            $repetition = $row['repetition'];

            $oldcategoryid = $row['categoryid'];
            $newcategoryid = 0;

            if ($oldcategoryid != 0)
            {

                for ($i = 0; $i < sizeof($oldcategorylist); $i++)
                {
                    if ($oldcategorylist[$i] == $oldcategoryid)
                        $newcategoryid = $newcategorylist[$i];
                }

            }

            $db->query("INSERT INTO workout_exercise (workoutid, exerciseid, setnum, repetition, categoryid) VALUES ($newworkoutid, $exerciseid, $setnum, $repetition, $newcategoryid)");

        }


        $response['success'] = 1;
        $response['message'] = "Successfully added!";

        echo json_encode($response);

        $db = null;
    }

    else
    {
        $response['success'] = 0;
        $response['message'] = "Posted variables are wrong!";
        echo json_encode($response);
    }

?>