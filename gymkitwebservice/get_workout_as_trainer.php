<?php

    $response = array();

    if (isset($_POST['gymid']))
    {
        $gymid = $_POST['gymid'];

        require_once('db_config.php');

        $result = $db->query("SELECT * FROM workout WHERE userid = 0 AND gymid = $gymid ORDER BY id DESC")->fetchAll(PDO::FETCH_ASSOC);

        if(isset($result[0]['id']))
        {
            $response['workout'] = array();

            foreach ($result as $row)
            {
                $workout = array();

                $workout['id'] = $row['id'];
                $workout['name'] = $row['name'];
                $workout['userid'] = $row['userid'];
                $workout['creatorid'] = $row['creatorid'];
                $workout['gymid'] = $row['gymid'];

                array_push($response['workout'], $workout);
            }

            $response['success'] = 1;

            echo json_encode($response);
        }

        else
        {
            $response['success'] = 0;
            $response['message'] = "No data found!";
            echo json_encode($response);
        }

        $db = null;

    }

    else
    {
        $response['success'] = 0;
        $response['message'] = "Posted variables are wrong!";
        echo json_encode($response);
    }

?>