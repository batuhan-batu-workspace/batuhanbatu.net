<?php

    $response = array();

    if (isset($_POST['id']))
    {
        $id = $_POST['id'];

        require_once('db_config.php');

        $result = $db->query("SELECT * FROM workout_category WHERE id = $id")->fetchAll(PDO::FETCH_ASSOC);

        if(isset($result[0]['id']))
        {
            $response['category'] = array();

            foreach ($result as $row)
            {
                $category = array();

                $category['id'] = $row['id'];
                $category['workoutid'] = $row['workoutid'];
                $category['name'] = $row['name'];

                array_push($response['category'], $category);
            }

            $response['success'] = 1;

            echo json_encode($response);
        }

        else
        {
            $response['success'] = 0;
            $response['message'] = "No data found!";
            echo json_encode($response);
        }

        $db = null;

    }

    else
    {
        $response['success'] = 0;
        $response['message'] = "Posted variables are wrong!";
        echo json_encode($response);
    }

?>