<?php

    $response = array();

    if (isset($_POST['userid']) and isset($_POST['code']))
    {
        $userid = $_POST['userid'];
        $code = $_POST['code'];

        require_once('db_config.php');

        $result = $db->query("SELECT * FROM gym_login WHERE code = $code AND activation = 1")->fetchAll(PDO::FETCH_ASSOC);

        if(isset($result[0]['id']))
        {
            $gymid = 0;
            $gymloginid = 0;

            foreach ($result as $row)
            {
                $gymid = $row['gymid'];
                $gymloginid = $row['id'];
            }

            if ($db->query("UPDATE gym_login SET activation = 0 WHERE id = $gymloginid"))
            {
                if ($db->query("UPDATE user SET gymid = $gymid, usertype = 0, activation = 0 WHERE id = $userid"))
                {
                    $response['success'] = 1;
                    $response['message'] = "Successfully added!";

                    echo json_encode($response);
                }
                else
                {
                    $response['success'] = 0;
                    $response['message'] = "Error!";
                    echo json_encode($response);
                }
            }
            else
            {
                $response['success'] = 0;
                $response['message'] = "Error!";
                echo json_encode($response);
            }

        }

        else
        {
            $response['success'] = 0;
            $response['message'] = "No data found!";
            echo json_encode($response);
        }

        $db = null;

    }

    else
    {
        $response['success'] = 0;
        $response['message'] = "Posted variables are wrong!";
        echo json_encode($response);
    }

?>