<?php

    $response = array();

    if (isset($_POST['gymid']) and isset($_POST['userid']))
    {
        $gymid = $_POST['gymid'];
        $userid = $_POST['userid'];

        $eacharticleid = 0;

        require_once('db_config.php');

        $result = $db->query("SELECT * FROM saved_article WHERE userid = $userid ORDER BY articleid DESC")->fetchAll(PDO::FETCH_ASSOC);

        if(isset($result[0]['userid']))
        {
            $response['article'] = array();

            foreach ($result as $row)
            {
                $eacharticleid = $row['articleid'];
                $result2 = $db->query("SELECT * FROM article WHERE id = $eacharticleid AND (gymid = $gymid OR gymid = 0)")->fetchAll(PDO::FETCH_ASSOC);
                if(isset($result2[0]['id']))
                {
                    
                    foreach ($result2 as $row2)
                    {
                        $article = array();

                        $article['id'] = $row2['id'];
                        $article['title'] = $row2['title'];
                        $article['photo'] = $row2['photo'];
                        $article['content'] = $row2['content'];
                        $article['date'] = $row2['date'];

                        array_push($response['article'], $article);
                    }

                    
                }
                else
                {
                    $response['success'] = 0;
                    $response['message'] = "No data found!";
                    echo json_encode($response);
                }
            }

            $response['success'] = 1;

            echo json_encode($response);
        }

        else
        {
            $response['success'] = 0;
            $response['message'] = "No data found!";
            echo json_encode($response);
        }

        $db = null;

    }

    else
    {
        $response['success'] = 0;
        $response['message'] = "Posted variables are wrong!";
        echo json_encode($response);
    }

?>