<?php

    $response = array();

    if (isset($_POST['firstid']) and isset($_POST['secondid']))
    {
        $firstid = $_POST['firstid'];
        $secondid = $_POST['secondid'];
        
        $weight1 = 0;
        $weight2 = 0;
        $fluid1 = 0;
        $fluid2 = 0;
        $oil1 = 0;
        $oil2 = 0;
        $muscle1 = 0;
        $muscle2 = 0;
        

        require_once('db_config.php');

        $result = $db->query("SELECT * FROM body_size WHERE id = $firstid")->fetchAll(PDO::FETCH_ASSOC);

        if(isset($result[0]['id']))
        {
            $response['body_size'] = array();

            foreach ($result as $row)
            {
                $bodysize = array();

                $weight1 = $row['weight'];
                $fluid1 = $row['fluid'];
                $oil1 = $row['oil'];
                $muscle1 = $row['muscle'];
                
                $bodysize['name1'] = $row['name'];
                $bodysize['weight1'] = $row['weight'];
                $bodysize['fluid1'] = $row['fluid'];
                $bodysize['oil1'] = $row['oil'];
                $bodysize['muscle1'] = $row['muscle'];

                $result2 = $db->query("SELECT * FROM body_size WHERE id = $secondid")->fetchAll(PDO::FETCH_ASSOC);
                if(isset($result2[0]['id']))
                {
                    foreach ($result2 as $row2)
                    {
                        $weight2 = $row2['weight'];
                        $fluid2 = $row2['fluid'];
                        $oil2 = $row2['oil'];
                        $muscle2 = $row2['muscle'];
                        
                        $bodysize['name2'] = $row2['name'];
                        $bodysize['weight2'] = $row2['weight'];
                        $bodysize['fluid2'] = $row2['fluid'];
                        $bodysize['oil2'] = $row2['oil'];
                        $bodysize['muscle2'] = $row2['muscle'];
                    }
                }

                if ($weight1 > $weight2)
                    $bodysize['weightt'] = -1;
                else if ($weight1 == $weight2)
                    $bodysize['weightt'] = 0;
                else
                    $bodysize['weightt'] = 1;

                if ($fluid1 > $fluid2)
                    $bodysize['fluidd'] = -1;
                else if ($fluid1 == $fluid2)
                    $bodysize['fluidd'] = 0;
                else
                    $bodysize['fluidd'] = 1;

                if ($oil1 > $oil2)
                    $bodysize['oill'] = -1;
                else if ($oil1 == $oil2)
                    $bodysize['oill'] = 0;
                else
                    $bodysize['oill'] = 1;

                if ($muscle1 > $muscle2)
                    $bodysize['musclee'] = -1;
                else if ($muscle1 == $muscle2)
                    $bodysize['musclee'] = 0;
                else
                    $bodysize['musclee'] = 1;


                array_push($response['body_size'], $bodysize);
            }

            $response['success'] = 1;

            echo json_encode($response);
        }

        else
        {
            $response['success'] = 0;
            $response['message'] = "No data found!";
            echo json_encode($response);
        }

        $db = null;

    }

    else
    {
        $response['success'] = 0;
        $response['message'] = "Posted variables are wrong!";
        echo json_encode($response);
    }

?>