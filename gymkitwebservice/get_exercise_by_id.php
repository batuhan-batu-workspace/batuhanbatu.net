<?php

    $response = array();

    if (isset($_POST['id']))
    {
        $id = $_POST['id'];

        require_once('db_config.php');

        $result = $db->query("SELECT * FROM exercise WHERE id = $id")->fetchAll(PDO::FETCH_ASSOC);

        if(isset($result[0]['id']))
        {
            $response['exercise'] = array();

            foreach ($result as $row)
            {
                $exercise = array();

                $exercise['id'] = $row['id'];
                $exercise['name'] = str_replace("'", "~", $row['name']);
                $exercise['groupid'] = $row['groupid'];
                $exercise['gif'] = $row['gif'];
                $exercise['photo1'] = $row['photo1'];
                $exercise['photo2'] = $row['photo2'];
                $exercise['how'] = str_replace("'", "~", $row['how']);

                array_push($response['exercise'], $exercise);
            }

            $response['success'] = 1;

            echo json_encode($response);
        }

        else
        {
            $response['success'] = 0;
            $response['message'] = "No data found!";
            echo json_encode($response);
        }

        $db = null;

    }

    else
    {
        $response['success'] = 0;
        $response['message'] = "Posted variables are wrong!";
        echo json_encode($response);
    }

?>