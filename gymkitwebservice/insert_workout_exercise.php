<?php

    $response = array();

    if (isset($_POST['workoutid']) and isset($_POST['exerciseid']) and isset($_POST['categoryid']))
    {
        $workoutid = $_POST['workoutid'];
        $exerciseid = $_POST['exerciseid'];
        $categoryid = $_POST['categoryid'];

        require_once('db_config.php');


        if($db->query("INSERT INTO workout_exercise (workoutid, exerciseid, setnum, repetition, categoryid) VALUES ($workoutid, $exerciseid, 0, 0, $categoryid)"))
        {
            $response['success'] = 1;
            $response['message'] = "Successfully added!";

            echo json_encode($response);
        }

        else
        {
            $response['success'] = 0;
            $response['message'] = "Error!";
        }



        $db = null;

    }

    else
    {
        $response['success'] = 0;
        $response['message'] = "Posted variables are wrong!";
        echo json_encode($response);
    }

?>