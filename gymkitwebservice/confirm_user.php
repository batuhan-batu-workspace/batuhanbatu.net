<?php

    $response = array();
    
    if (isset($_POST['id']))
    {
        $id = $_POST['id'];
    
        require_once('db_config.php');
    
        if($db->query("UPDATE user SET activation = 1, usertype = 0 WHERE id = $id"))
        {
            $response['success'] = 1;
            $response['message'] = "Successfully updated!";
    
            echo json_encode($response);
        }
    
        else
        {
            $response['success'] = 0;
            $response['message'] = "No record found!";
        }
    
        $db = null;
    
    }
    
    else
    {
        $response['success'] = 0;
        $response['message'] = "Posted variables are wrong!";
        echo json_encode($response);
    }

?>