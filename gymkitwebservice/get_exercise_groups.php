<?php

    $response = array();

    require_once('db_config.php');

    $result = $db->query("SELECT * FROM exercise_group ORDER BY id ASC")->fetchAll(PDO::FETCH_ASSOC);

    if(isset($result[0]['id']))
    {
        $response['group'] = array();

        foreach ($result as $row)
        {
            $group = array();

            $group['id'] = $row['id'];
            $group['name'] = $row['name'];
            $group['photo'] = $row['photo'];

            array_push($response['group'], $group);
        }

        $response['success'] = 1;

        echo json_encode($response);
    }

    else
    {
        $response['success'] = 0;
        $response['message'] = "No data found!";
        echo json_encode($response);
    }

    $db = null;

?>