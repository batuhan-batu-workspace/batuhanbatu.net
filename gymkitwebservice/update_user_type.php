<?php

    $response = array();

    if (isset($_POST['id']) and isset($_POST['usertype']))
    {
        $id = $_POST['id'];
        $usertype = $_POST['usertype'];

        require_once('db_config.php');

        if ($usertype == 1)
        {
            if($db->query("UPDATE user SET usertype = 0 WHERE id = $id"))
            {
                $response['success'] = 1;
                $response['message'] = "Successfully updated!";

                echo json_encode($response);
            }

            else
            {
                $response['success'] = 0;
                $response['message'] = "Error!";
            }
        }

        else
        {
            if($db->query("UPDATE user SET usertype = 1 WHERE id = $id"))
            {
                $response['success'] = 1;
                $response['message'] = "Successfully updated!";

                echo json_encode($response);
            }

            else
            {
                $response['success'] = 0;
                $response['message'] = "Error!";
            }
        }

        $db = null;

    }

    else
    {
        $response['success'] = 0;
        $response['message'] = "Posted variables are wrong!";
        echo json_encode($response);
    }

?>