<?php

    $response = array();

    if (isset($_POST['gymid']) and isset($_POST['text']))
    {
        $gymid = $_POST['gymid'];
        $text = $_POST['text'];

        require_once('db_config.php');

        $result = $db->query("SELECT article.* FROM article LEFT JOIN user ON article.authorid = user.id WHERE (article.gymid = $gymid OR article.gymid = 0) AND (article.title LIKE '%$text%' OR article.content LIKE '%$text%' OR CONCAT(user.fname, \" \", user.lname) LIKE '%$text%') ORDER BY article.id DESC")->fetchAll(PDO::FETCH_ASSOC);

        if(isset($result[0]['id']))
        {
            $response['article'] = array();

            foreach ($result as $row)
            {
                $article = array();

                $article['id'] = $row['id'];
                $article['title'] = str_replace("'", "~", $row['title']);
                $article['photo'] = $row['photo'];
                $article['content'] = str_replace("'", "~", $row['content']);
                $article['date'] = $row['date'];

                array_push($response['article'], $article);
            }

            $response['success'] = 1;

            echo json_encode($response);
        }

        else
        {
            $response['success'] = 0;
            $response['message'] = "No data found!";
            echo json_encode($response);
        }

        $db = null;

    }

    else
    {
        $response['success'] = 0;
        $response['message'] = "Posted variables are wrong!";
        echo json_encode($response);
    }

?>