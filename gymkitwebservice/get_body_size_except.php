<?php

    $response = array();

    if (isset($_POST['userid']) and isset($_POST['firstid']))
    {
        $userid = $_POST['userid'];
        $firstid = $_POST['firstid'];

        require_once('db_config.php');

        $result = $db->query("SELECT * FROM body_size WHERE userid = $userid AND id NOT IN ($firstid)")->fetchAll(PDO::FETCH_ASSOC);
    
        if(isset($result[0]['id']))
        {
            $response['body_size'] = array();

            foreach ($result as $row)
            {
                $bodysize = array();

                $bodysize['id'] = $row['id'];
                $bodysize['name'] = $row['name'];
                $bodysize['weight'] = $row['weight'];
                $bodysize['fluid'] = $row['fluid'];
                $bodysize['oil'] = $row['oil'];
                $bodysize['muscle'] = $row['muscle'];
                $bodysize['userid'] = $row['userid'];
                $bodysize['creatorid'] = $row['creatorid'];

                array_push($response['body_size'], $bodysize);
            }

            $response['success'] = 1;

            echo json_encode($response);
        }

        else
        {
            $response['success'] = 0;
            $response['message'] = "No data found!";
            echo json_encode($response);
        }

        $db = null;

    }

    else
    {
        $response['success'] = 0;
        $response['message'] = "Posted variables are wrong!";
        echo json_encode($response);
    }

?>