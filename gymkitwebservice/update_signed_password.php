<?php

    $response = array();

    if (isset($_POST['userid']) and isset($_POST['oldpassword']) and isset($_POST['newpassword']))
    {
        $userid = $_POST['userid'];
        $oldpassword = $_POST['oldpassword'];
        $newpassword = $_POST['newpassword'];
        
        $realpassword = "";

        require_once('db_config.php');

        $result = $db->query("SELECT * FROM user WHERE id = $userid")->fetchAll(PDO::FETCH_ASSOC);
        
        foreach ($result as $row)
        {
            $realpassword = $row['password'];
        }
            
        
        if ($oldpassword == $realpassword)
        {
            if($db->query("UPDATE user SET password = '$newpassword' WHERE id = $userid"))
            {
                $response['success'] = 1;
                $response['message'] = "Successfully updated!";

                echo json_encode($response);
            }
            else
            {
                $response['success'] = 0;
                $response['message'] = "Error!";
            }
        }

        else
        {
            $response['success'] = 0;
            $response['message'] = "Password is wrong!";
        }

        $db = null;

    }

    else
    {
        $response['success'] = 0;
        $response['message'] = "Posted variables are wrong!";
        echo json_encode($response);
    }

?>