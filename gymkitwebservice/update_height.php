<?php

    $response = array();

    if (isset($_POST['userid']) and isset($_POST['height']))
    {
        $userid = $_POST['userid'];
        $height = $_POST['height'];

        require_once('db_config.php');


        if($db->query("UPDATE user SET height = $height WHERE id = $userid"))
        {
            $response['success'] = 1;
            $response['message'] = "Successfully updated!";

            echo json_encode($response);
        }

        else
        {
            $response['success'] = 0;
            $response['message'] = "Error!";
        }

        $db = null;

    }

    else
    {
        $response['success'] = 0;
        $response['message'] = "Posted variables are wrong!";
        echo json_encode($response);
    }

?>