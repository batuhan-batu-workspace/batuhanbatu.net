<?php

    $response = array();

    if (isset($_POST['authorid']) and isset($_POST['name']) and isset($_POST['description']) and isset($_POST['content']) and isset($_POST['price']) and isset($_POST['tags']))
    {
        $authorid = $_POST['authorid'];
        $name = $_POST['name'];
        $description = $_POST['description'];
        $content = $_POST['content'];
        $price = $_POST['price'];
        $tags = $_POST['tags'];

        require_once('db_config.php');
        
        if($db->query("INSERT INTO poetry (authorid, name, description, content, price, tags) VALUES ($authorid, '$name', '$description', '$content', $price, '$tags')"))
        {
            $response['success'] = 1;
            $response['message'] = "Successfully added!";

            echo json_encode($response);
        }

        else
        {
            $response['success'] = 0;
            $response['message'] = "Couldn't add!";
        }

        mysqli_close($connection);
    }

    else
    {
        $response['success'] = 0;
        $response['message'] = "Posted variables are wrong!";
        echo json_encode($response);
    }

?>