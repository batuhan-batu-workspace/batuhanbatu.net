<?php

    $response = array();

    if (isset($_POST['id']))
    {
        $id = $_POST['id'];

        require_once('db_config.php');

        $result = $db->query("SELECT SUM(point) as sumpoint FROM buying WHERE poetryid IN (SELECT id FROM poetry WHERE authorid = $id)")->fetchAll(PDO::FETCH_ASSOC);

        $response['scores'] = array();

        foreach ($result as $row)
        {
            $scores = array();

            if($row['sumpoint'] != null)
                $scores['point'] = $row['sumpoint'];
            else
                $scores['point'] = 0;

            array_push($response['scores'], $scores);
        }

        $response['success'] = 1;

        echo json_encode($response);


        $db = null;

    }

    else
    {
        $response['success'] = 0;
        $response['message'] = "Posted variables are wrong!";
        echo json_encode($response);
    }

?>