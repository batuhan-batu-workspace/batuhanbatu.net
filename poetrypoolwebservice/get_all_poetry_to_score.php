<?php

    $response = array();

    require_once('db_config.php');
    $result = $db->query("SELECT * FROM poetry ORDER BY id DESC")->fetchAll(PDO::FETCH_ASSOC);

    if(isset($result[0]['id']))
    {
        $response['poetry'] = array();

        foreach ($result as $row)
        {
            $poetry = array();

            $poetry['id'] = $row['id'];
            $poetry['authorid'] = $row['authorid'];

            $authorid = $poetry['authorid'];

            $result2 = $db->query("SELECT * FROM user WHERE id = $authorid")->fetchAll(PDO::FETCH_ASSOC);
            foreach ($result2 as $row2)
            {
                $poetry['authorname'] = $row2['fname'].' '.$row2['lname'];
            }
            
            $result3 = $db->query("SELECT SUM(point) as sumpoint FROM buying WHERE poetryid IN (SELECT id FROM poetry WHERE authorid = $authorid)")->fetchAll(PDO::FETCH_ASSOC);
            foreach ($result3 as $row3)
            {
                if ($row3['sumpoint'] != null)
                    $poetry['totalpointofthepoet'] = $row3['sumpoint'];
                else
                    $poetry['totalpointofthepoet'] = "0";
            }

            $poetry['name'] = $row['name'];
            $poetry['description'] = $row['description'];
            $poetry['content'] = $row['content'];
            $poetry['price'] = $row['price'];
            $poetry['tags'] = $row['tags'];
            $poetry['date'] = $row['date'];

            array_push($response['poetry'], $poetry);

        }
        
        for ($i = 0; $i < sizeof($response['poetry']); $i++)
        {
            for ($j = $i; $j < sizeof($response['poetry']); $j++)
            {
                if ($response['poetry'][$i]['totalpointofthepoet'] < $response['poetry'][$j]['totalpointofthepoet'])
                {
                    $emptyVariable1 = $response['poetry'][$i]['id'];
                    $emptyVariable2 = $response['poetry'][$i]['authorid'];
                    $emptyVariable3 = $response['poetry'][$i]['authorname'];
                    $emptyVariable4 = $response['poetry'][$i]['totalpointofthepoet'];
                    $emptyVariable5 = $response['poetry'][$i]['name'];
                    $emptyVariable6 = $response['poetry'][$i]['description'];
                    $emptyVariable7 = $response['poetry'][$i]['content'];
                    $emptyVariable8 = $response['poetry'][$i]['price'];
                    $emptyVariable9 = $response['poetry'][$i]['tags'];
                    $emptyVariable10 = $response['poetry'][$i]['date'];
                    
                    /*******************************************/

                    $response['poetry'][$i]['id'] = $response['poetry'][$j]['id'];
                    $response['poetry'][$i]['authorid'] = $response['poetry'][$j]['authorid'];
                    $response['poetry'][$i]['authorname'] = $response['poetry'][$j]['authorname'];
                    $response['poetry'][$i]['totalpointofthepoet'] = $response['poetry'][$j]['totalpointofthepoet'];
                    $response['poetry'][$i]['name'] = $response['poetry'][$j]['name'];
                    $response['poetry'][$i]['description'] = $response['poetry'][$j]['description'];
                    $response['poetry'][$i]['content'] = $response['poetry'][$j]['content'];
                    $response['poetry'][$i]['price'] = $response['poetry'][$j]['price'];
                    $response['poetry'][$i]['tags'] = $response['poetry'][$j]['tags'];
                    $response['poetry'][$i]['date'] = $response['poetry'][$j]['date'];

                    /*******************************************/

                    $response['poetry'][$j]['id'] = $emptyVariable1;
                    $response['poetry'][$j]['authorid'] = $emptyVariable2;
                    $response['poetry'][$j]['authorname'] = $emptyVariable3;
                    $response['poetry'][$j]['totalpointofthepoet'] = $emptyVariable4;
                    $response['poetry'][$j]['name'] = $emptyVariable5;
                    $response['poetry'][$j]['description'] = $emptyVariable6;
                    $response['poetry'][$j]['content'] = $emptyVariable7;
                    $response['poetry'][$j]['price'] = $emptyVariable8;
                    $response['poetry'][$j]['tags'] = $emptyVariable9;
                    $response['poetry'][$j]['date'] = $emptyVariable10;
                    
                }
                
                else if ($response['poetry'][$i]['id'] < $response['poetry'][$j]['id'])
                {
                    $emptyVariable1 = $response['poetry'][$i]['id'];
                    $emptyVariable2 = $response['poetry'][$i]['authorid'];
                    $emptyVariable3 = $response['poetry'][$i]['authorname'];
                    $emptyVariable4 = $response['poetry'][$i]['totalpointofthepoet'];
                    $emptyVariable5 = $response['poetry'][$i]['name'];
                    $emptyVariable6 = $response['poetry'][$i]['description'];
                    $emptyVariable7 = $response['poetry'][$i]['content'];
                    $emptyVariable8 = $response['poetry'][$i]['price'];
                    $emptyVariable9 = $response['poetry'][$i]['tags'];
                    $emptyVariable10 = $response['poetry'][$i]['date'];

                    /*******************************************/

                    $response['poetry'][$i]['id'] = $response['poetry'][$j]['id'];
                    $response['poetry'][$i]['authorid'] = $response['poetry'][$j]['authorid'];
                    $response['poetry'][$i]['authorname'] = $response['poetry'][$j]['authorname'];
                    $response['poetry'][$i]['totalpointofthepoet'] = $response['poetry'][$j]['totalpointofthepoet'];
                    $response['poetry'][$i]['name'] = $response['poetry'][$j]['name'];
                    $response['poetry'][$i]['description'] = $response['poetry'][$j]['description'];
                    $response['poetry'][$i]['content'] = $response['poetry'][$j]['content'];
                    $response['poetry'][$i]['price'] = $response['poetry'][$j]['price'];
                    $response['poetry'][$i]['tags'] = $response['poetry'][$j]['tags'];
                    $response['poetry'][$i]['date'] = $response['poetry'][$j]['date'];

                    /*******************************************/

                    $response['poetry'][$j]['id'] = $emptyVariable1;
                    $response['poetry'][$j]['authorid'] = $emptyVariable2;
                    $response['poetry'][$j]['authorname'] = $emptyVariable3;
                    $response['poetry'][$j]['totalpointofthepoet'] = $emptyVariable4;
                    $response['poetry'][$j]['name'] = $emptyVariable5;
                    $response['poetry'][$j]['description'] = $emptyVariable6;
                    $response['poetry'][$j]['content'] = $emptyVariable7;
                    $response['poetry'][$j]['price'] = $emptyVariable8;
                    $response['poetry'][$j]['tags'] = $emptyVariable9;
                    $response['poetry'][$j]['date'] = $emptyVariable10;
                }
            }
            
        }

        $response['success'] = 1;

        echo json_encode($response);
    }

    else
    {
        $response['success'] = 0;
        $response['message'] = "No data found!";
        echo json_encode($response);
    }

    $db = null;

?>