<?php

    $response = array();

    if (isset($_POST['poetryid']))
    {
        $poetryid = $_POST['poetryid'];

        require_once('db_config.php');

        $result = $db->query("SELECT * FROM buying WHERE poetryid = $poetryid")->fetchAll(PDO::FETCH_ASSOC);

        if(isset($result[0]['id']))
        {
            $response['buying'] = array();

            foreach ($result as $row)
            {
                $buying = array();

                $buying['id'] = $row['id'];
                $buying['poetryid'] = $row['poetryid'];
                $buying['buyerid'] = $row['buyerid'];
                $buying['point'] = $row['point'];

                array_push($response['buying'], $buying);
            }

            $response['success'] = 1;

            echo json_encode($response);
        }

        else
        {
            $response['success'] = 0;
            $response['message'] = "No data found!";
            echo json_encode($response);
        }

        $db = null;

    }

    else
    {
        $response['success'] = 0;
        $response['message'] = "Posted variables are wrong!";
        echo json_encode($response);
    }

?>