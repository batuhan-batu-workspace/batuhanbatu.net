<?php

    $response = array();
    
    if (isset($_POST['buyerid']))
    {
        $buyerid = $_POST['buyerid'];
    
        require_once('db_config.php');
    
        $result = $db->query("SELECT * FROM buying WHERE buyerid = $buyerid ORDER BY id DESC")->fetchAll(PDO::FETCH_ASSOC);
        if(isset($result[0]['id']))
        {

            $response['poetry'] = array();
    
            foreach ($result as $row)
            {

                $poetryid = $row['poetryid'];
                $result2 = $db->query("SELECT * FROM poetry WHERE id = $poetryid")->fetchAll(PDO::FETCH_ASSOC);
                if(isset($result2[0]['id']))
                {
                    
                    foreach ($result2 as $row2)
                    {
                        $poetry = array();

                        $poetry['id'] = $row2['id'];
                        $poetry['authorid'] = $row2['authorid'];

                        $authorid = $poetry['authorid'];
                        $result3 = $db->query("SELECT * FROM user WHERE id = $authorid")->fetchAll(PDO::FETCH_ASSOC);
                        foreach ($result3 as $row3)
                        {
                            $poetry['authorname'] = $row3['fname'].' '.$row3['lname'];
                        }

                        $poetry['name'] = $row2['name'];
                        $poetry['description'] = $row2['description'];
                        $poetry['content'] = $row2['content'];
                        $poetry['price'] = $row2['price'];
                        $poetry['tags'] = $row2['tags'];
                        $poetry['date'] = $row2['date'];

                        array_push($response['poetry'], $poetry);
                    }
                    
                }

            }

            $response['success'] = 1;

            echo json_encode($response);

        }
    
        else
        {
            $response['success'] = 0;
            $response['message'] = "No data found!";
            echo json_encode($response);
        }
    
        $db = null;
    
    }
    
    else
    {
        $response['success'] = 0;
        $response['message'] = "Posted variables are wrong!";
        echo json_encode($response);
    }

?>