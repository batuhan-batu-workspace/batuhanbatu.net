<?php

    $response = array();
    
    if (isset($_POST['email']))
    {
        $email = $_POST['email'];

        require_once('db_config.php');
    
        $result = $db->query("SELECT * FROM user WHERE email = '$email'")->fetchAll(PDO::FETCH_ASSOC);

        if(isset($result[0]['id']))
        {
            $response['user'] = array();

            foreach ($result as $row)
            {
                $user = array();
    
                $user['id'] = $row['id'];
                $user['fname'] = $row['fname'];
                $user['lname'] = $row['lname'];
                $user['email'] = $row['email'];
                $user['password'] = $row['password'];
                $user['date'] = $row['date'];
                $user['brightcoin'] = $row['brightcoin'];
                $user['rustycoin'] = $row['rustycoin'];
    
                array_push($response['user'], $user);
            }
    
            $response['success'] = 1;
    
            echo json_encode($response);
        }
    
        else
        {
            $response['success'] = 0;
            $response['message'] = "No data found!";
            echo json_encode($response);
        }

        $db = null;
    
    }
    
    else
    {
        $response['success'] = 0;
        $response['message'] = "Posted variables are wrong!";
        echo json_encode($response);
    }

?>