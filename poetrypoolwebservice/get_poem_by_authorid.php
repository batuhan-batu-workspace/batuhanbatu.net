<?php

    $response = array();
    
    if (isset($_POST['authorid']))
    {
        $authorid = $_POST['authorid'];

        require_once('db_config.php');

        $result = $db->query("SELECT * FROM poetry WHERE authorid = $authorid ORDER BY id DESC")->fetchAll(PDO::FETCH_ASSOC);

        if(isset($result[0]['id']))
        {
            $response['poetry'] = array();

            foreach ($result as $row)
            {
                $poetry = array();

                $poetry['id'] = $row['id'];
                $poetry['authorid'] = $row['authorid'];

                $authorid = $poetry['authorid'];
                $result2 = $db->query("SELECT * FROM user WHERE id = $authorid")->fetchAll(PDO::FETCH_ASSOC);
                foreach ($result2 as $row2)
                {
                    $poetry['authorname'] = $row2['fname'].' '.$row2['lname'];
                }

                $poetry['name'] = $row['name'];
                $poetry['description'] = $row['description'];
                $poetry['content'] = $row['content'];
                $poetry['price'] = $row['price'];
                $poetry['tags'] = $row['tags'];
                $poetry['date'] = $row['date'];

                array_push($response['poetry'], $poetry);
            }
    
            $response['success'] = 1;
    
            echo json_encode($response);
        }
    
        else
        {
            $response['success'] = 0;
            $response['message'] = "No data found!";
            echo json_encode($response);
        }

        $db = null;
    
    }
    
    else
    {
        $response['success'] = 0;
        $response['message'] = "Posted variables are wrong!";
        echo json_encode($response);
    }

?>