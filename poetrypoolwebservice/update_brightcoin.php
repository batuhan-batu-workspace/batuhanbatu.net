<?php

    $response = array();
    
    if (isset($_POST['id']) and isset($_POST['brightcoin']))
    {
        $id = $_POST['id'];
        $brightcoin = $_POST['brightcoin'];
        $buyersbrightcoin = 0;

        require_once('db_config.php');

        $result = $db->query("SELECT * FROM user WHERE id = $id")->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row)
        {
            $buyersbrightcoin = $row['brightcoin'];
        }

        $buyersbrightcoin = $buyersbrightcoin + $brightcoin;

        if($db->query("UPDATE user SET brightcoin = $buyersbrightcoin WHERE id = $id"))
        {
            $response['success'] = 1;
            $response['message'] = "Successfully updated!";

            echo json_encode($response);
        }

        else
        {
            $response['success'] = 0;
            $response['message'] = "No record found!";
        }

        $db = null;
        
    }
    
    else
    {
        $response['success'] = 0;
        $response['message'] = "Posted variables are wrong!";
        echo json_encode($response);
    }

?>