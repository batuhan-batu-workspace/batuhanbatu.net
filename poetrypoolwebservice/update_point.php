<?php

    $response = array();

    if (isset($_POST['buyerid']) and isset($_POST['poetryid']) and isset($_POST['point']))
    {
        $buyerid = $_POST['buyerid'];
        $poetryid = $_POST['poetryid'];
        $point = $_POST['point'];
    
        require_once('db_config.php');
    
        if($db->query("UPDATE buying SET point = $point WHERE buyerid = $buyerid AND poetryid = $poetryid"))
        {
            $response['success'] = 1;
            $response['message'] = "Successfully updated!";
    
            echo json_encode($response);
        }
    
        else
        {
            $response['success'] = 0;
            $response['message'] = "No record found!";
        }
    
        $db = null;
    
    }
    
    else
    {
        $response['success'] = 0;
        $response['message'] = "Posted variables are wrong!";
        echo json_encode($response);
    }

?>