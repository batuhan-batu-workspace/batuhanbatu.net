<?php

    $response = array();

    if (isset($_POST['id']))
    {
        $id = $_POST['id'];
        require_once('db_config.php');
        if($db->query("DELETE FROM buying WHERE id = $id"))
        {
            $response['success'] = 1;
            $response['message'] = "Successfully deleted!";

            echo json_encode($response);
        }

        else
        {
            $response['success'] = 0;
            $response['message'] = "No record found!";
        }
        
        $db = null;
    }

    else
    {
        $response['success'] = 0;
        $response['message'] = "Posted variables are wrong!";
        echo json_encode($response);
    }

?>