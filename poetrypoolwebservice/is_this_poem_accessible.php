<?php

    $response = array();
    
    if (isset($_POST['buyerid']) and isset($_POST['poetryid']))
    {
        $buyerid = $_POST['buyerid'];
        $poetryid = $_POST['poetryid'];
    
        require_once('db_config.php');
    
        $result = $db->query("SELECT * FROM buying WHERE buyerid = $buyerid AND poetryid = $poetryid")->fetchAll(PDO::FETCH_ASSOC);
        $result2 = $db->query("SELECT * FROM poetry WHERE authorid = $buyerid AND id = $poetryid")->fetchAll(PDO::FETCH_ASSOC);
    
        if(isset($result[0]['id']))
        {
            $response['buying'] = array();
    
            foreach ($result as $row)
            {
                $buying = array();
    
                $buying['poetryid'] = $row['poetryid'];
    
                array_push($response['buying'], $buying);
            }
    
            $response['success'] = 1;
    
            echo json_encode($response);
        }
        
        else if (isset($result2[0]['id']))
        {
            $response['buying'] = array();

            foreach ($result2 as $row2)
            {
                $buying = array();

                $buying['poetryid'] = $row2['id'];

                array_push($response['buying'], $buying);
            }

            $response['success'] = 1;

            echo json_encode($response);
        }
    
        else
        {
            $response['success'] = 0;
            $response['message'] = "No data found!";
            echo json_encode($response);
        }
    
        $db = null;
    
    }
    
    else
    {
        $response['success'] = 0;
        $response['message'] = "Posted variables are wrong!";
        echo json_encode($response);
    }

?>