<?php

    $response = array();
    
    if (isset($_POST['id']) and isset($_POST['rustycoin']))
    {
        $id = $_POST['id'];
        $rustycoin = $_POST['rustycoin'];
    
        require_once('db_config.php');
    
        if($db->query("UPDATE user SET rustycoin = $rustycoin WHERE id = $id"))
        {
            $response['success'] = 1;
            $response['message'] = "Successfully updated!";
    
            echo json_encode($response);
        }
    
        else
        {
            $response['success'] = 0;
            $response['message'] = "No record found!";
        }
    
        $db = null;
    
    }
    
    else
    {
        $response['success'] = 0;
        $response['message'] = "Posted variables are wrong!";
        echo json_encode($response);
    }

?>